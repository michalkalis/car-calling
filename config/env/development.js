const dbAdminUsername = "admin"
const dbAdminPassword = "o3T-2pR-8Cn-Wou"
const dbName = "car-calling"

export default {
  session: 'secret-boilerplate-token',
  token: 'secret-jwt-token',
  database: `mongodb://${dbAdminUsername}:${dbAdminPassword}@ds137540.mlab.com:37540/car-calling`
}
