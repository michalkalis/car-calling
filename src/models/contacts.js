import mongoose from 'mongoose'

const Contact = new mongoose.Schema({
  phone: { type: Number, required: true },
  firstname: { type: String },
  lastname: { type: String }
})

export default mongoose.model('contact', Contact)
